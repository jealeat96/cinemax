import { Component, OnInit } from '@angular/core';
import { LandingPageService } from 'src/app/services/landing-page.service';
import { Movie } from 'src/app/models/movie.models';

declare var $: any;

@Component({
  selector: 'app-movies-filter',
  templateUrl: './movies-filter.component.html',
  styleUrls: ['./movies-filter.component.sass']
})
export class MoviesFilterComponent implements OnInit {
  fechas: Date[] = [];
  peliculas: Movie[] = [];

  formFilter = {
    date: new Date(),
    pelicula: '',
    formato: '',
    hora: '',
  };

  constructor(public landingPageService: LandingPageService) { }

  ngOnInit() {
    $('select').formSelect();
    this.createDates();
    this.getMovies();
  }

  createDates() {
    for (let i = 0; i < 7; i++) {
      const date: Date = new Date();
      date.setDate(date.getDate() + i);
      this.fechas.push(date);
    }
  }

  getMovies() {
    this.landingPageService.getMovies().subscribe((movies: Movie[]) => {
      this.peliculas = movies.filter(mov => mov.cartelera);
    });
  }

}
