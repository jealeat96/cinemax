import { Component, OnInit } from '@angular/core';
import { LandingPageService } from 'src/app/services/landing-page.service';
import { Movie } from 'src/app/models/movie.models';

@Component({
  selector: 'app-weekly-billboards',
  templateUrl: './weekly-billboards.component.html',
  styleUrls: ['./weekly-billboards.component.sass']
})
export class WeeklyBillboardsComponent implements OnInit {
  peliculas: Movie[] = [];

  constructor(public landingPageService: LandingPageService) { }

  ngOnInit() {
    this.getWeeklyBillboards();
  }

  getWeeklyBillboards() {
    this.landingPageService.getMovies().subscribe((movies: Movie[]) => {
      this.peliculas = movies.filter(mov => mov.cartelera);
    });
  }

}
