import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavBarSocialComponent } from './navbar-social.component';
import { LandingPageService } from 'src/app/services/landing-page.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { of } from 'rxjs';
import { SocialNetwork } from './social-network';

describe('NavBarSocialComponent', () => {
  let component: NavBarSocialComponent;
  let fixture: ComponentFixture<NavBarSocialComponent>;
  let service: LandingPageService;
  const mockResponse: SocialNetwork[] = [
    {
      clase: 'fab fa-facebook-f',
      estado: true,
      nombre: 'Facebook',
      url: 'www.facebook.com'
    },
    {
      clase: 'fab fa-twitter',
      estado: true,
      nombre: 'Twwiter',
      url: 'www.twitter.com'
    },
    {
      clase: 'fab fa-youtube',
      estado: true,
      nombre: 'Youtube',
      url: 'www.youtube.com'
    },
    {
      clase: 'fab fa-instagram',
      estado: true,
      nombre: 'Instagram',
      url: 'www.instragram.com'
    }
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavBarSocialComponent ],
      imports: [ HttpClientTestingModule ],
      // schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavBarSocialComponent);
    component = fixture.componentInstance;
    service = component.landingPageService;
    fixture.detectChanges();
  });

  it('Debe crear el componente', () => {
    expect(component).toBeTruthy();
  });

  it('Debe inyectar LandingPageService', () => {
    expect(service).toBeTruthy();
  });

  it('Debe llenar la variable socialNetworks al llamar getSocialNetworks() de landingPageService', () => {
    spyOn(service, 'getSocialNetworks').and.returnValue(of(mockResponse));

    component.ngOnInit();

    expect(component.socialNetworks).toBe(mockResponse);
  });

  it('Debe llamar getSocialNetworks() en el ngOnInit', () => {
    spyOn(component, 'getSocialNetworks').and.callThrough();
    spyOn(service, 'getSocialNetworks').and.returnValue(of(mockResponse));

    component.ngOnInit();

    expect(component.getSocialNetworks).toHaveBeenCalled();
  });
});
