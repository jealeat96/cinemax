import { Component, OnInit } from '@angular/core';
import { LandingPageService } from 'src/app/services/landing-page.service';
import { SocialNetwork } from './social-network';

@Component({
  selector: 'app-nav-bar-social',
  templateUrl: './navbar-social.component.html',
  styleUrls: ['./navbar-social.component.sass']
})
export class NavBarSocialComponent implements OnInit {
  socialNetworks: SocialNetwork[] = [];

  constructor(public landingPageService: LandingPageService) { }

  ngOnInit() {
    this.getSocialNetworks();
  }

  getSocialNetworks() {
    this.landingPageService.getSocialNetworks().subscribe(redes => {
      this.socialNetworks = redes;
    });
  }

}
