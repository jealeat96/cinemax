export interface SocialNetwork {
  clase: string;
  estado: boolean;
  nombre: string;
  url: string;
}
