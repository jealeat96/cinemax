import { Component, OnInit } from '@angular/core';
import { LandingPageService } from 'src/app/services/landing-page.service';
import { Movie } from 'src/app/models/movie.models';

@Component({
  selector: 'app-premieres',
  templateUrl: './premieres.component.html',
  styleUrls: ['./premieres.component.sass']
})
export class PremieresComponent implements OnInit {
  peliculas: Movie[] = [];

  constructor(public landingPageService: LandingPageService) { }

  ngOnInit() {
    this.getPremieres();
  }

  getPremieres() {
    this.landingPageService.getMovies().subscribe((movies: Movie[]) => {
      this.peliculas = movies.filter(mov => !mov.cartelera);
    });
  }

}
